# Task-manager #
Консольное приложение с возможностью параметризированного запуска.





## Системные требования: ##
  * OpenJDK11

## Стек технологий: ##
  * Apache Maven 3.6.3  

## Разработчик: ##
  * Имя: Смольянинов С.
  * Email: smolianinov@gmail.com
    
## Команды сборки приложения: ##
  `  mvn clear - удаление артефактов  `

  `  mvn compile - компилирование проекта  `

  `  mvn test - тестирование с помощью JUnit тестов  `

  `  mvn package - создание файла jar файла  `

  `  mvn install - копирование файла jar в локальный репозиторий  `

  `  mvn deploy - публикация файла в удаленный репозиторий  `

## Команды запуска приложения: ##
  `  java -jar task-manager-1.0.0.jar  `

  `  java -jar task-manager-1.0.0.jar version  `

  `  java -jar task-manager-1.0.0.jar help  `

  `  java -jar task-manager-1.0.0.jar about  `
    